FROM php:7.1-apache
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \ 
    && docker-php-ext-install zip

ENV GITHUB_TOKEN your_key_here
ENV SEARCH_CODE_ADAPTER github