<?php

date_default_timezone_set("America/Sao_paulo");

require_once __DIR__.'/vendor/autoload.php';

use SearchCode\AdapterManager;
use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

$app["debug"] = true;

$app->get('/search/', function(Request $request) use($app) {
    $adapter = AdapterManager::getAdapter(getenv('SEARCH_CODE_ADAPTER'));
    $adapter->parseRequest($request);
    $data    = $adapter->searchCode();

    return $app->json($data);
});

$app->run();