<?php

namespace SearchCode\Adapter;

use SearchCode\AdapterInterface;
use SearchCode\Exception\AdapterException;
use Symfony\Component\HttpFoundation\Request;
use SearchCode\Adapter\AbstractAdapter;

use Github;

class GithubAdapter extends AbstractAdapter implements AdapterInterface 
{
    private $github;
    private $token;

    public function __construct() {
        $this->github = new \Github\Client();
    }

    private function parseReturn(Array $data) {
        $returnData = array();
        foreach($data['items'] as $key => $value) {
            $returnData[$key]['owner_name']      = $data['items'][$key]['repository']['owner']['login'];
            $returnData[$key]['repository_name'] = $data['items'][$key]['repository']['name'];
            $returnData[$key]['file_name']       = $data['items'][$key]['name'];
        }
        return $returnData;
    }

    public function configure(){
        if(!getenv('GITHUB_TOKEN')){
            throw new AdapterException(
                'The requested adapter needs "GITHUB_TOKEN", please provider your token.'
            );
        }
        $this->token = getenv('GITHUB_TOKEN');
        return $this;
    }

    public function authenticate() {
        $this->github->authenticate($this->token, null, Github\Client::AUTH_HTTP_TOKEN);
        return $this;
    }

    public function parseRequest(Request $request) {
        if(!$request->get('q')) {
            throw new AdapterException(
                'The query string parameter "q", can not be invalid.'
            );
        }
        $this->query = $request->get('q');
        $this->sort  = $request->get('sort') ? $request->get('sort'): $this->sort;
        $this->page  = $request->get('page') ? $request->get('page'): $this->page;
        $this->hits  = $request->get('hits') ? $request->get('hits'): $this->hits;
    }

    public function searchCode() {
        $paginator = new Github\ResultPager($this->github);
        $searchObj = $this->github->api('search');

        $searchObj->setPage($this->page);
        $searchObj->setPerPage($this->hits);

        $data = $paginator->fetch($searchObj, 'code', array($this->query, $this->sort));

        $data = $this->parseReturn($data);

        return $data;
    }
}