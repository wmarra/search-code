<?php

namespace SearchCode;

use Symfony\Component\HttpFoundation\Request;

interface AdapterInterface
{
    public function configure();
    public function authenticate();
    public function searchCode();
}